#!/bin/sh
### BEGIN INIT INFO
# Provides: ${WILDFLY_SERVICE}
# Required-Start: $local_fs $remote_fs $network $syslog
# Required-Stop: $local_fs $remote_fs $network $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start/Stop ${WILDFLY_FILENAME}
### END INIT INFO

JBOSS_HOME="/opt/Programs/WildFly/ImpalaRemittance"
WILDFLY_USER="administrator"
WILDFLY_DIR=${JBOSS_HOME}
 
case "$1" in
start)
echo "Starting ${WILDFLY_FILENAME}..."
start-stop-daemon --start --background --chuid $WILDFLY_USER --exec $WILDFLY_DIR/bin/standalone.sh
exit $?
;;
stop)
echo "Stopping ${WILDFLY_FILENAME}..."
# The port mentioned on localhost must much the Wildfly standalone configuration file: standalone.xml
# The line to match is as follows:
# <socket-binding name="management-http" interface="management" port="${jboss.management.http.port:10001}"/>
start-stop-daemon --start --quiet --background --chuid $WILDFLY_USER --exec $WILDFLY_DIR/bin/jboss-cli.sh -- --connect controller=localhost:10023 command=:shutdown
exit $?
;;
reload)
echo "Reloading ${WILDFLY_FILENAME}..."
# The port mentioned on localhost must much the Wildfly standalone configuration file: standalone.xml
# The line to match is as follows:
# <socket-binding name="management-http" interface="management" port="${jboss.management.http.port:10001}"/>
start-stop-daemon --start --quiet --background --chuid $WILDFLY_USER --exec $WILDFLY_DIR/bin/jboss-cli.sh -- --connect controller=localhost:10023 command=:reload
exit $?
;;
log)
echo "Showing server.log..."
tail -500f $WILDFLY_DIR/standalone/log/server.log
;;
*)
echo "Usage: /etc/init.d/wildfly {start|stop|reload|log}"
exit 1
;;
esac
exit 0
EOF
sed -i -e 's,${WILDFLY_USER},'$WILDFLY_USER',g; s,${WILDFLY_FILENAME},'$WILDFLY_FILENAME',g; s,${WILDFLY_SERVICE},'$WILDFLY_SERVICE',g; s,${WILDFLY_DIR},'$WILDFLY_DIR',g' /etc/init.d/$WILDFLY_SERVICE
fi
 
chmod 755 /etc/init.d/$WILDFLY_SERVICE
 
if [ ! -z "$WILDFLY_SERVICE_CONF" ]; then
echo "Configuring service..."
echo JBOSS_HOME=\"$WILDFLY_DIR\" > $WILDFLY_SERVICE_CONF
echo JBOSS_USER=$WILDFLY_USER >> $WILDFLY_SERVICE_CONF
echo STARTUP_WAIT=$WILDFLY_STARTUP_TIMEOUT >> $WILDFLY_SERVICE_CONF
echo SHUTDOWN_WAIT=$WILDFLY_SHUTDOWN_TIMEOUT >> $WILDFLY_SERVICE_CONF
fi
 
echo "Configuring application server..."
sed -i -e 's,<deployment-scanner path="deployments" relative-to="jboss.server.base.dir" scan-interval="5000"/>,<deployment-scanner path="deployments" relative-to="jboss.server.base.dir" scan-interval="5000" deployment-timeout="'$WILDFLY_STARTUP_TIMEOUT'"/>,g' $WILDFLY_DIR/standalone/configuration/standalone.xml
sed -i -e 's,<inet-address value="${jboss.bind.address:127.0.0.1}"/>,<any-address/>,g' $WILDFLY_DIR/standalone/configuration/standalone.xml
sed -i -e 's,<socket-binding name="ajp" port="${jboss.ajp.port:8042}"/>,<socket-binding name="ajp" port="${jboss.ajp.port:28042}"/>,g' $WILDFLY_DIR/standalone/configuration/standalone.xml
sed -i -e 's,<socket-binding name="http" port="${jboss.http.port:8113}"/>,<socket-binding name="http" port="${jboss.http.port:28113}"/>,g' $WILDFLY_DIR/standalone/configuration/standalone.xml
sed -i -e 's,<socket-binding name="https" port="${jboss.https.port:8476}"/>,<socket-binding name="https" port="${jboss.https.port:28476}"/>,g' $WILDFLY_DIR/standalone/configuration/standalone.xml
sed -i -e 's,<socket-binding name="osgi-http" interface="management" port="8123"/>,<socket-binding name="osgi-http" interface="management" port="28123"/>,g' $WILDFLY_DIR/standalone/configuration/standalone.xml
 
service $WILDFLY_SERVICE start
 
echo "Done."
